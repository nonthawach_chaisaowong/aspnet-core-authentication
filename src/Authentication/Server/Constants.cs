﻿namespace Server
{
    public static class Constants
    {
        // For experiment purpose, shold store in secure place
        public const string Issuer = Audiance;
        public const string Audiance = "https://localhost:44323/";
        public const string Secret = "this_is_very_long_secret";
    }
}
